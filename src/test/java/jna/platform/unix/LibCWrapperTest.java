/*
 * Copyright 2014 Yann D'Isanto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jna.platform.unix;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yann D'Isanto
 */
public class LibCWrapperTest {

    public LibCWrapperTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of uname method, of class LibCWrapper.
     */
    @Test
    public void testUname() {
        // Arrange

        // Act
        UNameInfo info = LibCWrapper.uname();

        // Assert
        assertNotNull(info);
        assertNotNull(info.getSysname());
        assertNotNull(info.getRelease());
        assertNotNull(info.getVersion());
        assertNotNull(info.getMachine());
        assertNotNull(info.getDomainname());
        assertNotNull(info.getNodename());
    }

    /**
     * Test of sysinfo method, of class LibCWrapper.
     */
    @Test
    public void testSysinfo() {
        // Arrange

        // Act
        SysInfo info = LibCWrapper.sysinfo();

        // Assert
        // no exception is enough for now
        assertNotNull(info);
        assertTrue(info.getUpTime() > 0);

        assertTrue(info.getTotalRam() > 0);
        assertTrue(info.getFreeRam() >= 0);
        assertTrue(info.getSharedRam() >= 0);
        assertTrue(info.getBufferRam() >= 0);
        assertTrue(info.getTotalSwap() >= 0);
        assertTrue(info.getFreeSwap() >= 0);
        assertTrue(info.getProcesses() >= 0);
        assertTrue(info.getTotalHigh() >= 0);
        assertTrue(info.getFreeHigh() >= 0);
        assertTrue(info.getMemoryUnit() > 0);
    }

}
