/*
 * Copyright 2014 Yann D'Isanto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jna.platform.unix;

import java.util.Arrays;
import static jna.platform.unix.LibC.sysinfo.computePaddingArraySize;
import lombok.Value;

/**
 *
 * @author Yann D'Isanto
 */
@Value
public class SysInfo {

    /**
     * Seconds since boot
     */
    private long upTime;

    /**
     * 1, 5, and 15 minute load averages
     */
    private long[] loads/* = new long[3]*/;

    /**
     * Total usable main memory size
     */
    private long totalRam;

    /**
     * Available memory size
     */
    private long freeRam;

    /**
     * Amount of shared memory
     */
    private long sharedRam;

    /**
     * Memory used by buffers
     */
    private long bufferRam;

    /**
     * Total swap space size
     */
    private long totalSwap;

    /**
     * swap space still available
     */
    private long freeSwap;

    /**
     * Number of current processes
     */
    private short processes;

    /**
     * Padding needed for m68k
     */
    private short pad;

    /**
     * Total high memory size
     */
    private long totalHigh;

    /**
     * Available high memory size
     */
    private long freeHigh;

    /**
     * Memory unit size in bytes
     */
    private int memoryUnit;

    /**
     * Padding: libc5 uses this..
     */
    private byte[] padding;

    public SysInfo(long upTime, long[] loads, long totalRam, long freeRam, long sharedRam, long bufferRam, long totalSwap, long freeSwap, short processes, short pad, long totalHigh, long freeHigh, int memoryUnit, byte[] _f) {
        this.upTime = upTime;
        this.loads = Arrays.copyOfRange(loads, 0, 3);
        this.totalRam = totalRam;
        this.freeRam = freeRam;
        this.sharedRam = sharedRam;
        this.bufferRam = bufferRam;
        this.totalSwap = totalSwap;
        this.freeSwap = freeSwap;
        this.processes = processes;
        this.pad = pad;
        this.totalHigh = totalHigh;
        this.freeHigh = freeHigh;
        this.memoryUnit = memoryUnit;
        this.padding = Arrays.copyOf(_f, computePaddingArraySize());
    }

    public static SysInfo of(LibC.sysinfo info) {
        return new SysInfo(
                info.uptime, 
                info.loads, 
                info.totalram, 
                info.freeram, 
                info.sharedram, 
                info.bufferram, 
                info.totalswap, 
                info.freeswap, 
                info.procs, 
                info.pad, 
                info.totalhigh, 
                info.freehigh, 
                info.mem_unit, 
                info._f);
    }
    
}
