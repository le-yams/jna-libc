/*
 * Copyright 2014 yann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jna.platform.unix;

import gnu.libc.ErrorCode;

/**
 *
 * @author Yann D'Isanto
 */
public final class LibCWrapper {
    
    
    public static UNameInfo uname() {
        LibC.utsname.ByReference info = new LibC.utsname.ByReference();
        if(LibC.INSTANCE.uname(info) < 0) {
            ErrorCode.EFAULT.throwMe();
        }
        return UNameInfo.of(info);
    }

    public static SysInfo sysinfo() {
        LibC.sysinfo.ByReference info = new LibC.sysinfo.ByReference();
        if(LibC.INSTANCE.sysinfo(info) < 0) {
            ErrorCode.EFAULT.throwMe();
        }
        return SysInfo.of(info);
    }

    private LibCWrapper() {
    }
        
}
