/*
 * Copyright 2014 Yann D'Isanto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jna.platform.unix;

import static util.StringUtil.nullTerminatedString;
import lombok.Value;

/**
 *
 * @author Yann D'Isanto
 */
@Value
public class UNameInfo {

    /**
     * The name of the operating system in use.
     */
    private String sysname;

    /**
     * The host name of this particular computer.
     */
    private String nodename;

    /**
     * The current release level of the operating system implementation.
     */
    private String release;

    /**
     * The current version level within the release of the operating system.
     */
    private String version;

    /**
     * A description of the type of hardware that is in use.
     *
     * GNU uses a three-part name to describe a system configuration; the three
     * parts are cpu, manufacturer and system-type, and they are separated with
     * dashes. Any possible combination of three names is potentially
     * meaningful, but most such combinations are meaningless in practice and
     * even the meaningful ones are not necessarily supported by any particular
     * GNU program.
     *
     * Since the value in machine is supposed to describe just the hardware, it
     * consists of the first two parts of the configuration name:
     * ‘cpu-manufacturer’. For example, it might be one of these:
     *
     * "sparc-sun", "i386-anything", "m68k-hp", "m68k-sony", "m68k-sun",
     * "mips-dec"
     */
    private String machine;

    /**
     * The NIS or YP domain name.
     */
    private String domainname;
    
    
    public static UNameInfo of(LibC.utsname utsname) {
        return new UNameInfo(
                nullTerminatedString(utsname.sysname), 
                nullTerminatedString(utsname.nodename), 
                nullTerminatedString(utsname.release), 
                nullTerminatedString(utsname.version), 
                nullTerminatedString(utsname.machine), 
                nullTerminatedString(utsname.domainname)
        );
    }
}
