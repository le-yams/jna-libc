/*
 * Copyright 2014 yann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jna.platform.unix;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;

/**
 *
 * @author Yann D'Isanto
 */
public interface LibC extends Library {

    LibC INSTANCE = (LibC) Native.loadLibrary("c", LibC.class);

    /**
     * The {@code uname} function fills in the structure pointed to by info with
     * information about the operating system and host machine.
     *
     * @param info a {@code utsname} structure pointer to store the information
     * into.
     * @return a non-negative value if the data was successfully stored, -1 if
     * an error occurred. The only error possible is EFAULT, which we normally
     * don’t mention as it is always a possibility.
     */
    int uname(utsname.ByReference info);

    /**
     * The {@code get_nprocs_conf} function returns the number of processors the
     * operating system configured.
     *
     * This function is a GNU extension.
     *
     * @return the number of processors the operating system configured
     */
    int get_nprocs_conf();

    /**
     * The {@code get_nprocs} function returns the number of available
     * processors.
     *
     * This function is a GNU extension.
     *
     * @return the number of available processors
     */
    int get_nprocs();

    /**
     * The {@code utsname} structure is used to hold information returned by the
     * {@code name} function.
     */
    static class utsname extends Structure {

        static final int _UTSNAME_LENGTH = 65;

        /**
         * This is the name of the operating system in use.
         */
        public byte[] sysname = new byte[_UTSNAME_LENGTH];

        /**
         * This is the host name of this particular computer. In the GNU C
         * Library, the value is the same as that returned by gethostname; see
         * Host Identification.
         *
         * gethostname() is implemented with a call to uname().
         */
        public byte[] nodename = new byte[_UTSNAME_LENGTH];

        /**
         * This is the current release level of the operating system
         * implementation.
         */
        public byte[] release = new byte[_UTSNAME_LENGTH];

        /**
         * This is the current version level within the release of the operating
         * system.
         */
        public byte[] version = new byte[_UTSNAME_LENGTH];

        /**
         * This is a description of the type of hardware that is in use.
         *
         * Some systems provide a mechanism to interrogate the kernel directly
         * for this information. On systems without such a mechanism, the GNU C
         * Library fills in this field based on the configuration name that was
         * specified when building and installing the library.
         *
         * GNU uses a three-part name to describe a system configuration; the
         * three parts are cpu, manufacturer and system-type, and they are
         * separated with dashes. Any possible combination of three names is
         * potentially meaningful, but most such combinations are meaningless in
         * practice and even the meaningful ones are not necessarily supported
         * by any particular GNU program.
         *
         * Since the value in machine is supposed to describe just the hardware,
         * it consists of the first two parts of the configuration name:
         * ‘cpu-manufacturer’. For example, it might be one of these:
         *
         * "sparc-sun", "i386-anything", "m68k-hp", "m68k-sony", "m68k-sun",
         * "mips-dec"
         */
        public byte[] machine = new byte[_UTSNAME_LENGTH];

        /**
         * This is the NIS or YP domain name. It is the same value returned by
         * getdomainname; see Host Identification. This element is a relatively
         * recent invention and use of it is not as portable as use of the rest
         * of the structure.
         */
        public byte[] domainname = new byte[_UTSNAME_LENGTH];

        public static class ByReference extends utsname implements Structure.ByReference {
        }
    }

    /**
     * The {@code sysinfo} structure is used to hold information returned by the
     * {@code sysinfo} function.
     * 
     * Since Linux 2.3.23 (i386), 2.3.48 (all architectures)
     */
    static class sysinfo extends Structure {

        /**
         * Seconds since boot
         */
        public long uptime;

        /**
         * 1, 5, and 15 minute load averages
         */
        public long[] loads = new long[3];

        /**
         * Total usable main memory size
         */
        public long totalram;

        /**
         * Available memory size
         */
        public long freeram;

        /**
         * Amount of shared memory
         */
        public long sharedram;

        /**
         * Memory used by buffers
         */
        public long bufferram;

        /**
         * Total swap space size
         */
        public long totalswap;

        /**
         * swap space still available
         */
        public long freeswap;

        /**
         * Number of current processes
         */
        public short procs;

        /**
         * Padding needed for m68k
         */
        public short pad;

        /**
         * Total high memory size
         */
        public long totalhigh;

        /**
         * Available high memory size
         */
        public long freehigh;

        /**
         * Memory unit size in bytes
         */
        public int mem_unit;

        /**
         * Padding: libc5 uses this..
         */
        public byte[] _f = new byte[computePaddingArraySize()];

        public static class ByReference extends sysinfo implements Structure.ByReference {
        }

        public static int computePaddingArraySize() {
            int size = 20 - 2 * Native.LONG_SIZE - Native.getNativeSize(Integer.TYPE);
            return size > 0 ? size : 1;
        }
    }

    /**
     * Returns information on overall system statistics.
     *
     * @param info
     * @return
     */
    int sysinfo(sysinfo.ByReference info);
}
